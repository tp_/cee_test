<?php

namespace Tp\ServiceBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Tp\ServiceBundle\Entity\Serwis;
use Tp\ServiceBundle\Form\SerwisType;

/**
 * Serwis controller.
 *
 */
class SerwisController extends Controller
{

    /**
     * Lists all Serwis entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TpServiceBundle:Serwis')->findAll();

        return $this->render('TpServiceBundle:Serwis:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Serwis entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Serwis();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('serwis_show', array('id' => $entity->getId())));
        }

        return $this->render('TpServiceBundle:Serwis:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Serwis entity.
     *
     * @param Serwis $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Serwis $entity)
    {
        $form = $this->createForm(new SerwisType(), $entity, array(
            'action' => $this->generateUrl('serwis_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Serwis entity.
     *
     */
    public function newAction()
    {
        $entity = new Serwis();
        $form   = $this->createCreateForm($entity);

        return $this->render('TpServiceBundle:Serwis:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Serwis entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TpServiceBundle:Serwis')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Serwis entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TpServiceBundle:Serwis:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Serwis entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TpServiceBundle:Serwis')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Serwis entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TpServiceBundle:Serwis:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Serwis entity.
    *
    * @param Serwis $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Serwis $entity)
    {
        $form = $this->createForm(new SerwisType(), $entity, array(
            'action' => $this->generateUrl('serwis_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Serwis entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TpServiceBundle:Serwis')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Serwis entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('serwis_edit', array('id' => $id)));
        }

        return $this->render('TpServiceBundle:Serwis:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Serwis entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TpServiceBundle:Serwis')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Serwis entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('serwis'));
    }

    /**
     * Creates a form to delete a Serwis entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('serwis_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
