<?php

namespace Tp\ServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Serwis
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Tp\ServiceBundle\Entity\SerwisRepository")
 */
class Serwis
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="wpis", type="string", length=255)
     */
    private $wpis;

    /**
     * @ORM\OneToMany(targetEntity="Tp\ServiceBundle\Entity\Todo", mappedBy="object", cascade={"all"})
     */
    private $todos;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set wpis
     *
     * @param string $wpis
     * @return Serwis
     */
    public function setWpis($wpis)
    {
        $this->wpis = $wpis;

        return $this;
    }

    /**
     * Get wpis
     *
     * @return string 
     */
    public function getWpis()
    {
        return $this->wpis;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->todos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add todos
     *
     * @param \Tp\ServiceBundle\Entity\Todo $todos
     * @return Serwis
     */
    public function addTodo(\Tp\ServiceBundle\Entity\Todo $todos)
    {
        $this->todos[] = $todos;

        return $this;
    }

    /**
     * Remove todos
     *
     * @param \Tp\ServiceBundle\Entity\Todo $todos
     */
    public function removeTodo(\Tp\ServiceBundle\Entity\Todo $todos)
    {
        $this->todos->removeElement($todos);
    }

    /**
     * Get todos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTodos()
    {
        return $this->todos;
    }
}
