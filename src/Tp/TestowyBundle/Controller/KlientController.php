<?php

namespace Tp\TestowyBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Tp\TestowyBundle\Entity\Klient;
use Tp\TestowyBundle\Form\KlientType;

/**
 * Klient controller.
 *
 */
class KlientController extends Controller
{

    /**
     * Lists all Klient entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TpTestowyBundle:Klient')->findAll();

        return $this->render('TpTestowyBundle:Klient:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Klient entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Klient();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tp_klient_show', array('id' => $entity->getId())));
        }

        return $this->render('TpTestowyBundle:Klient:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Klient entity.
     *
     * @param Klient $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Klient $entity)
    {
        $form = $this->createForm(new KlientType(), $entity, array(
            'action' => $this->generateUrl('tp_klient_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Klient entity.
     *
     */
    public function newAction()
    {
        $entity = new Klient();
        $form   = $this->createCreateForm($entity);

        return $this->render('TpTestowyBundle:Klient:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Klient entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TpTestowyBundle:Klient')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Klient entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TpTestowyBundle:Klient:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Klient entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TpTestowyBundle:Klient')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Klient entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('TpTestowyBundle:Klient:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Klient entity.
    *
    * @param Klient $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Klient $entity)
    {
        $form = $this->createForm(new KlientType(), $entity, array(
            'action' => $this->generateUrl('tp_klient_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Klient entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TpTestowyBundle:Klient')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Klient entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('tp_klient_edit', array('id' => $id)));
        }

        return $this->render('TpTestowyBundle:Klient:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Klient entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TpTestowyBundle:Klient')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Klient entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('tp_klient'));
    }

    /**
     * Creates a form to delete a Klient entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tp_klient_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
